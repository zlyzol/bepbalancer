package mainbalancer

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"github.com/binance-chain/go-sdk/client/websocket"

	"gitlab.com/zlyzol/bepbalancer/balancer"
	"gitlab.com/zlyzol/bepbalancer/common"
	"gitlab.com/zlyzol/bepbalancer/dexscn"
	"gitlab.com/zlyzol/bepbalancer/poolscn"
)

type MainBalancer struct {
	ctx         *common.Context
	poolScanner *poolscn.PoolScanner
	dexSacnner  *dexscn.DexScanner
	balancer    *balancer.Balancer
}

func NewMainBalancer() (*MainBalancer, error) {
	log.Print("MainBalancer.NewMainBalancer start")
	defer log.Print("MainBalancer.NewMainBalancer end")

	var err error
	mb := MainBalancer{}
	// context init
	mb.ctx, err = common.NewContext("")
	if err != nil {
		return nil, fmt.Errorf("Context init fail. Err: %v", err)
	}
	// logging init
	logFile, err := os.OpenFile(mb.ctx.Config.LogFile, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		return nil, fmt.Errorf("Cannot open log file. Error: %v", err)
	}
	mw := io.MultiWriter(os.Stdout, logFile)
	_ = mw
	log.SetOutput(mw)
	//log.SetOutput(logFile)

	// read pools
	mb.poolScanner, err = poolscn.NewPoolScanner(mb.ctx)
	if err != nil {
		return nil, fmt.Errorf("NewPoolScanner init fail. Err: %v", err)
	}
	// DEX price/orderbooks scan
	mb.dexSacnner, err = dexscn.NewDexScanner(mb.ctx)
	if err != nil {
		return nil, fmt.Errorf("NewDexScanner init fail. Err: %v", err)
	}
	// pool balancer
	mb.balancer, err = balancer.NewBalancer(mb.ctx, mb.poolScanner, mb.dexSacnner)
	if err != nil {
		return nil, fmt.Errorf("NewBalancer init fail. Err: %v", err)
	}
	return &mb, nil
}

func (mb *MainBalancer) Scan() error {
	log.Print("MainBalancer.Scan start")
	defer log.Print("MainBalancer.Scan end")

	if err := mb.check(0); err != nil {
		return err
	}
	tooManyErr := 500
	errorCnt := 0
	stopAfterH := 24
	timeout := time.After(time.Duration(stopAfterH) * time.Hour)
	tickerAll := time.NewTicker(10 * time.Second)      // periodically check pools and orderbooks
	tickerOrderbook := time.NewTicker(2 * time.Second) // periodically check orderbooks

	chPoolEvent := make(chan *websocket.AccountEvent, 10) // Thorchain vault account event receiver channel
	err := mb.ctx.Dex.SubscribeAccountEvent(mb.ctx.ThorBNBAddr, nil, func(event *websocket.AccountEvent) { chPoolEvent <- event }, nil, nil)
	if err != nil {
		return fmt.Errorf("SubscribeAccountEvent error: %v", err)
	}

	for {
		select {
		case <-tickerAll.C:
			if err = mb.check(0); err != nil {
				log.Printf("MainBalancer SEVERE ERROR (%v-th): %v", errorCnt, err)
				errorCnt++
			}
			if errorCnt > tooManyErr {
				return fmt.Errorf("Too many errors")
			}
		case <-tickerOrderbook.C:
			if err = mb.check(2); err != nil {
				log.Printf("MainBalancer SEVERE ERROR (%v-th): %v", errorCnt, err)
				errorCnt++
			}
			if errorCnt > tooManyErr {
				return fmt.Errorf("Too many errors")
			}
		case <-chPoolEvent:
			if err = mb.check(1); err != nil {
				log.Printf("MainBalancer SEVERE ERROR (%v-th): %v", errorCnt, err)
				errorCnt++
			}
			if errorCnt > tooManyErr {
				return fmt.Errorf("Too many errors")
			}
		case <-timeout:
			log.Printf("End after %v hour", stopAfterH)
			return nil
		}
	}
}

func (mb *MainBalancer) check(what int) error { // 0 - check all, 1 - check pools, 2 - check orderbooks
	log.Print("MainBalancer.check start")
	defer log.Print("MainBalancer.check end")

	var err error
	changedPools := false
	changedOrderbooks := false
	if what == 0 || what == 1 {
		// get pools
		changedPools, err = mb.poolScanner.GetPools("")
		if err != nil {
			return fmt.Errorf("GetPools error: %v", err)
		}
	}
	if what == 0 || what == 2 {
		// get orderbooks
		changedOrderbooks, err = mb.dexSacnner.GetOrderbooks()
		if err != nil {
			return fmt.Errorf("GetOrderbooks error: %v", err)
		}
	}
	// if changed, try to balance pools
	if changedPools || changedOrderbooks {
		err := mb.balancer.Balance()
		if err != nil {
			return fmt.Errorf("balancer.Balance() error: %v", err)
		}
	}
	return nil
}
