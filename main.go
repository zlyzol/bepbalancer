package main

import (
	"log"
	"gitlab.com/zlyzol/bepbalancer/mainbalancer"
)

// TODO:
// 1. test arbitrage
// 2. solve BNB and BUSD balancing
// 3. for just balancing query the price from last trade price (not from orderbooks)

func main() {
	mainBalancer, err := mainbalancer.NewMainBalancer()
	if err != nil {
		log.Fatalf("Fatal error: %v", err)
	}
	if err := mainBalancer.Scan(); err != nil {
		log.Fatalf("Fatal error: %v", err)
	}

}
