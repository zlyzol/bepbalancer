package poolscn

import "gitlab.com/zlyzol/bepbalancer/common"

// PoolData - Pool data struct
type PoolData struct {
	AssetDepth common.Fixed8
	RuneDepth  common.Fixed8
	RunePrice  float64
	BasePrice  float64
}

// PoolsData - Data for asset's pools
type PoolsData map[string]PoolData

// AssetDetail defines model for AssetDetail.
type AssetDetail struct {
	Asset       *Asset   `json:"asset,omitempty"`
	DateCreated *int64   `json:"dateCreated,omitempty"`
	Logo        *string  `json:"logo,omitempty"`
	Name        *string  `json:"name,omitempty"`
	PriceRune   *float64 `json:"priceRune,omitempty"`
}

// Error defines model for Error.
type Error struct {
	Error string `json:"error"`
}

// PoolDetailOriginal defines model for PoolDetail.
type PoolDetailOriginal struct {
	Asset            *Asset   `json:"asset,omitempty"`
	AssetDepth       *int64   `json:"assetDepth,omitempty"`
	AssetROI         *float64 `json:"assetROI,omitempty"`
	AssetStakedTotal *int64   `json:"assetStakedTotal,omitempty"`
	BuyAssetCount    *int64   `json:"buyAssetCount,omitempty"`
	BuyFeeAverage    *int64   `json:"buyFeeAverage,omitempty"`
	BuyFeesTotal     *int64   `json:"buyFeesTotal,omitempty"`
	BuySlipAverage   *float64 `json:"buySlipAverage,omitempty"`
	BuyTxAverage     *int64   `json:"buyTxAverage,omitempty"`
	BuyVolume        *int64   `json:"buyVolume,omitempty"`
	PoolDepth        *int64   `json:"poolDepth,omitempty"`
	PoolFeeAverage   *int64   `json:"poolFeeAverage,omitempty"`
	PoolFeesTotal    *int64   `json:"poolFeesTotal,omitempty"`
	PoolROI          *float64 `json:"poolROI,omitempty"`
	PoolROI12        *float64 `json:"poolROI12,omitempty"`
	PoolSlipAverage  *float64 `json:"poolSlipAverage,omitempty"`
	PoolStakedTotal  *int64   `json:"poolStakedTotal,omitempty"`
	PoolTxAverage    *int64   `json:"poolTxAverage,omitempty"`
	PoolUnits        *int64   `json:"poolUnits,omitempty"`
	PoolVolume       *int64   `json:"poolVolume,omitempty"`
	PoolVolume24hr   *int64   `json:"poolVolume24hr,omitempty"`
	Price            *float64 `json:"price,omitempty"`
	RuneDepth        *int64   `json:"runeDepth,omitempty"`
	RuneROI          *float64 `json:"runeROI,omitempty"`
	RuneStakedTotal  *int64   `json:"runeStakedTotal,omitempty"`
	SellAssetCount   *int64   `json:"sellAssetCount,omitempty"`
	SellFeeAverage   *int64   `json:"sellFeeAverage,omitempty"`
	SellFeesTotal    *int64   `json:"sellFeesTotal,omitempty"`
	SellSlipAverage  *float64 `json:"sellSlipAverage,omitempty"`
	SellTxAverage    *int64   `json:"sellTxAverage,omitempty"`
	SellVolume       *int64   `json:"sellVolume,omitempty"`
	StakeTxCount     *int64   `json:"stakeTxCount,omitempty"`
	StakersCount     *int64   `json:"stakersCount,omitempty"`
	StakingTxCount   *int64   `json:"stakingTxCount,omitempty"`
	Status           *string  `json:"status,omitempty"`
	SwappersCount    *int64   `json:"swappersCount,omitempty"`
	SwappingTxCount  *int64   `json:"swappingTxCount,omitempty"`
	WithdrawTxCount  *int64   `json:"withdrawTxCount,omitempty"`
}

// PoolDetail defines model for PoolDetail.
type PoolDetail struct {
	Asset            *string `json:"asset,omitempty"`
	AssetDepth       *string `json:"assetDepth,omitempty"`
	AssetROI         *string `json:"assetROI,omitempty"`
	AssetStakedTotal *string `json:"assetStakedTotal,omitempty"`
	BuyAssetCount    *string `json:"buyAssetCount,omitempty"`
	BuyFeeAverage    *string `json:"buyFeeAverage,omitempty"`
	BuyFeesTotal     *string `json:"buyFeesTotal,omitempty"`
	BuySlipAverage   *string `json:"buySlipAverage,omitempty"`
	BuyTxAverage     *string `json:"buyTxAverage,omitempty"`
	BuyVolume        *string `json:"buyVolume,omitempty"`
	PoolDepth        *string `json:"poolDepth,omitempty"`
	PoolFeeAverage   *string `json:"poolFeeAverage,omitempty"`
	PoolFeesTotal    *string `json:"poolFeesTotal,omitempty"`
	PoolROI          *string `json:"poolROI,omitempty"`
	PoolROI12        *string `json:"poolROI12,omitempty"`
	PoolSlipAverage  *string `json:"poolSlipAverage,omitempty"`
	PoolStakedTotal  *string `json:"poolStakedTotal,omitempty"`
	PoolTxAverage    *string `json:"poolTxAverage,omitempty"`
	PoolUnits        *string `json:"poolUnits,omitempty"`
	PoolVolume       *string `json:"poolVolume,omitempty"`
	PoolVolume24hr   *string `json:"poolVolume24hr,omitempty"`
	Price            *string `json:"price,omitempty"`
	RuneDepth        *string `json:"runeDepth,omitempty"`
	RuneROI          *string `json:"runeROI,omitempty"`
	RuneStakedTotal  *string `json:"runeStakedTotal,omitempty"`
	SellAssetCount   *string `json:"sellAssetCount,omitempty"`
	SellFeeAverage   *string `json:"sellFeeAverage,omitempty"`
	SellFeesTotal    *string `json:"sellFeesTotal,omitempty"`
	SellSlipAverage  *string `json:"sellSlipAverage,omitempty"`
	SellTxAverage    *string `json:"sellTxAverage,omitempty"`
	SellVolume       *string `json:"sellVolume,omitempty"`
	StakeTxCount     *string `json:"stakeTxCount,omitempty"`
	StakersCount     *string `json:"stakersCount,omitempty"`
	StakingTxCount   *string `json:"stakingTxCount,omitempty"`
	Status           *string `json:"status,omitempty"`
	SwappersCount    *string `json:"swappersCount,omitempty"`
	SwappingTxCount  *string `json:"swappingTxCount,omitempty"`
	WithdrawTxCount  *string `json:"withdrawTxCount,omitempty"`
}

// MyPoolDetail - as it is on web API doc - not used - https://gitlab.com/thorchain/midgard/-/blob/master/api/rest/v1/specification/openapi-v1.0.0.yml
type MyPoolDetail struct {
	asset            *string  `json:"asset"`
	Status           *string  `json:"status",omitempty`
	price            *float64 `json:"price",omitempty`
	AssetStakedTotal *string  `json:"assetStakedTotal",omitempty`
	RuneStakedTotal  *string  `json:"runeStakedTotal",omitempty`
	poolStakedTotal  *string  `json:"poolStakedTotal",omitempty`
	assetDepth       *string  `json:"assetDepth",omitempty`
	runeDepth        *string  `json:"runeDepth",omitempty`
	poolDepth        *string  `json:"poolDepth",omitempty`
	poolUnits        *string  `json:"poolUnits",omitempty`
	sellVolume       *string  `json:"sellVolume",omitempty`
	buyVolume        *string  `json:"buyVolume",omitempty`
	poolVolume       *string  `json:"poolVolume",omitempty`
	poolVolume24hr   *string  `json:"poolVolume24hr",omitempty`
	sellTxAverage    *string  `json:"sellTxAverage",omitempty`
	buyTxAverage     *string  `json:"buyTxAverage",omitempty`
	poolTxAverage    *string  `json:"poolTxAverage",omitempty`
	sellSlipAverage  *float64 `json:"sellSlipAverage",omitempty`
	buySlipAverage   *float64 `json:"buySlipAverage",omitempty`
	poolSlipAverage  *float64 `json:"poolSlipAverage",omitempty`
	sellFeeAverage   *string  `json:"sellFeeAverage",omitempty`
	buyFeeAverage    *string  `json:"buyFeeAverage",omitempty`
	poolFeeAverage   *string  `json:"poolFeeAverage",omitempty`
	sellFeesTotal    *string  `json:"sellFeesTotal",omitempty`
	buyFeesTotal     *string  `json:"buyFeesTotal",omitempty`
	poolFeesTotal    *string  `json:"poolFeesTotal",omitempty`
	sellAssetCount   *string  `json:"sellAssetCount",omitempty`
	buyAssetCount    *string  `json:"buyAssetCount",omitempty`
	swappingTxCount  *string  `json:"swappingTxCount",omitempty`
	swappersCount    *string  `json:"swappersCount",omitempty`
	stakeTxCount     *string  `json:"stakeTxCount",omitempty`
	withdrawTxCount  *string  `json:"withdrawTxCount",omitempty`
	stakingTxCount   *string  `json:"stakingTxCount",omitempty`
	stakersCount     *string  `json:"stakersCount",omitempty`
	assetROI         *float64 `json:"assetROI",omitempty`
	runeROI          *float64 `json:"runeROI",omitempty`
	poolROI          *float64 `json:"poolROI",omitempty`
	poolROI12        *float64 `json:"poolROI12",omitempty`
}

// PoolsDetail defines model for array of PoolDetail.
type PoolsDetail []PoolDetail

// Stakers defines model for Stakers.
type Stakers string

// StakersAddressData defines model for StakersAddressData.
type StakersAddressData struct {
	PoolsArray  *[]Asset `json:"poolsArray,omitempty"`
	TotalEarned *int64   `json:"totalEarned,omitempty"`
	TotalROI    *float64 `json:"totalROI,omitempty"`
	TotalStaked *int64   `json:"totalStaked,omitempty"`
}

// StakersAssetData defines model for StakersAssetData.
type StakersAssetData struct {
	Asset           *Asset   `json:"asset,omitempty"`
	AssetEarned     *int64   `json:"assetEarned,omitempty"`
	AssetROI        *float64 `json:"assetROI,omitempty"`
	AssetStaked     *int64   `json:"assetStaked,omitempty"`
	DateFirstStaked *int64   `json:"dateFirstStaked,omitempty"`
	PoolEarned      *int64   `json:"poolEarned,omitempty"`
	PoolROI         *float64 `json:"poolROI,omitempty"`
	PoolStaked      *int64   `json:"poolStaked,omitempty"`
	RuneEarned      *int64   `json:"runeEarned,omitempty"`
	RuneROI         *float64 `json:"runeROI,omitempty"`
	RuneStaked      *int64   `json:"runeStaked,omitempty"`
	StakeUnits      *int64   `json:"stakeUnits,omitempty"`
}

// StatsData defines model for StatsData.
type StatsData struct {
	DailyActiveUsers   *int64 `json:"dailyActiveUsers,omitempty"`
	DailyTx            *int64 `json:"dailyTx,omitempty"`
	MonthlyActiveUsers *int64 `json:"monthlyActiveUsers,omitempty"`
	MonthlyTx          *int64 `json:"monthlyTx,omitempty"`
	PoolCount          *int64 `json:"poolCount,omitempty"`
	TotalAssetBuys     *int64 `json:"totalAssetBuys,omitempty"`
	TotalAssetSells    *int64 `json:"totalAssetSells,omitempty"`
	TotalDepth         *int64 `json:"totalDepth,omitempty"`
	TotalEarned        *int64 `json:"totalEarned,omitempty"`
	TotalStakeTx       *int64 `json:"totalStakeTx,omitempty"`
	TotalStaked        *int64 `json:"totalStaked,omitempty"`
	TotalTx            *int64 `json:"totalTx,omitempty"`
	TotalUsers         *int64 `json:"totalUsers,omitempty"`
	TotalVolume        *int64 `json:"totalVolume,omitempty"`
	TotalVolume24hr    *int64 `json:"totalVolume24hr,omitempty"`
	TotalWithdrawTx    *int64 `json:"totalWithdrawTx,omitempty"`
}

// TxDetails defines model for TxDetails.
type TxDetails struct {
	Date    *int64  `json:"date,omitempty"`
	Events  *Event  `json:"events,omitempty"`
	Gas     *Gas    `json:"gas,omitempty"`
	Height  *int64  `json:"height,omitempty"`
	In      *Tx     `json:"in,omitempty"`
	Options *Option `json:"options,omitempty"`
	Out     *Tx     `json:"out,omitempty"`
	Pool    *Asset  `json:"pool,omitempty"`
	Status  *string `json:"status,omitempty"`
	Type    *string `json:"type,omitempty"`
}

// Asset defines model for asset.
type Asset string

// OriginalAsset defines model for asset.
type OriginalAsset struct {
	Chain  *string `json:"chain,omitempty"`
	Symbol *string `json:"symbol,omitempty"`
	Ticker *string `json:"ticker,omitempty"`
}

// Coin defines model for coin.
type Coin struct {
	Amount *int64 `json:"amount,omitempty"`
	Asset  *Asset `json:"asset,omitempty"`
}

// Coins defines model for coins.
type Coins []Coin

// Event defines model for event.
type Event struct {
	Fee        *int64   `json:"fee,omitempty"`
	Slip       *float64 `json:"slip,omitempty"`
	StakeUnits *int64   `json:"stakeUnits,omitempty"`
}

// Gas defines model for gas.
type Gas struct {
	Amount *int64 `json:"amount,omitempty"`
	Asset  *Asset `json:"asset,omitempty"`
}

// Option defines model for option.
type Option struct {
	Asymmetry           *float64 `json:"asymmetry,omitempty"`
	PriceTarget         *int64   `json:"priceTarget,omitempty"`
	WithdrawBasisPoints *int64   `json:"withdrawBasisPoints,omitempty"`
}

// Tx defines model for tx.
type Tx struct {
	Address *string `json:"address,omitempty"`
	Coins   *Coins  `json:"coins,omitempty"`
	Memo    *string `json:"memo,omitempty"`
	TxID    *string `json:"txID,omitempty"`
}

// AssetsDetailedResponse defines model for AssetsDetailedResponse.
type AssetsDetailedResponse AssetDetail

// GeneralErrorResponse defines model for GeneralErrorResponse.
type GeneralErrorResponse Error

// PoolsDetailedResponse defines model for PoolsDetailedResponse.
type PoolsDetailedResponse PoolDetail

// PoolsResponse defines model for PoolsResponse.
type PoolsResponse []Asset

// StakersAddressDataResponse defines model for StakersAddressDataResponse.
type StakersAddressDataResponse StakersAddressData

// StakersAssetDataResponse defines model for StakersAssetDataResponse.
type StakersAssetDataResponse StakersAssetData

// StakersResponse defines model for StakersResponse.
type StakersResponse []Stakers

// StatsResponse defines model for StatsResponse.
type StatsResponse StatsData

// TxDetailedResponse defines model for TxDetailedResponse.
type TxDetailedResponse []TxDetails
