package poolscn

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	
	"gitlab.com/zlyzol/bepbalancer/common"
)

// PoolScanner - Midgard API pool scanner
type PoolScanner struct {
	ctx            *common.Context
	Pools          PoolsData
	BNBPriceInRune float64
}

// NewMidgardScanner - creates Midgard API object (without authentication)
func NewPoolScanner(ctx *common.Context) (*PoolScanner, error) {
	log.Print("NewPoolScanner start")
	defer log.Print("NewPoolScanner end")

	ps := PoolScanner{
		ctx:   ctx,
		Pools: make(PoolsData, len(ctx.Assets)),
	}
	return &ps, nil
}

// GetPools - updates pool data via midgard API
func (ps *PoolScanner) GetPools(asset string) (bool, error) {
	log.Print("GetPools start")
	defer log.Print("GetPools end")

	changed := false
	url := ps.ctx.MidgardURL + "/v1/pools/detail?asset="
	if asset != "" {
		url = url + asset
	} else {
		for i, a := range ps.ctx.Assets {
			if i == 0 {
				url = url + a
			} else {
				url = url + "," + a
			}
		}
	}
	log.Printf("GetPools url = %v\n", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return false, err
	}
	bytes, err := common.DoHttpRequest(req)
	if err != nil {
		return false, err
	}
	var data PoolsDetail
	err = json.Unmarshal(bytes, &data)
	if err != nil {
		return false, err
	}
	if asset == "" {
		found := false
		for _, pool := range data {
			if *pool.Asset == "BNB.BNB" {
				ad, _ := common.Fixed8ParseString(*pool.AssetDepth)
				rd, _ := common.Fixed8ParseString(*pool.RuneDepth)
				ps.BNBPriceInRune = common.Fx8ToFl64(rd) / common.Fx8ToFl64(ad)
				found = true
				break
			}
		}
		if !found {
			return false, fmt.Errorf("GetPools - Cannot find base %v pool!!!", "BNB.BNB")
		}
	}

	for _, pool := range data {
		asset := *pool.Asset
		if !ps.ctx.AssetSync[asset].CanWorkOn(3) {
			continue
		}

		//log.Printf("GetPools - Asset = %v RuneDepth = %v, AssetDepth = %v, (Orig pool)Price = %v", asset, *pool.RuneDepth, *pool.AssetDepth, *pool.Price)
		//f, _ := strconv.ParseFloat(*pool.Price, 64)
		ad, _ := common.Fixed8ParseString(*pool.AssetDepth)
		rd, _ := common.Fixed8ParseString(*pool.RuneDepth)
		d := PoolData{
			AssetDepth:	ad,
			RuneDepth:	rd,
			RunePrice:  common.Fx8ToFl64(rd) / common.Fx8ToFl64(ad),
			BasePrice:  common.Fx8ToFl64(rd) / common.Fx8ToFl64(ad) / ps.BNBPriceInRune,
		}
		if asset == ps.ctx.BNB {
			d.BasePrice = common.Fx8ToFl64(ad) / common.Fx8ToFl64(rd)
		}
		
		log.Printf("GetPools - Asset = %v RuneDepth = %v, AssetDepth = %v, (Orig pool)Price = %v, real Price (in rune) %v, basePrice (in BNB) %v", asset, d.RuneDepth, d.AssetDepth, *pool.Price, d.RunePrice, d.BasePrice)
		if ps.Pools[asset] != d {
			ps.Pools[asset] = d
			changed = true
		}
		defer ps.ctx.AssetSync[asset].WorkDone()
	}
	return changed, nil
}
