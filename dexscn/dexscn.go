package dexscn

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"

	"gitlab.com/zlyzol/bepbalancer/common"
)

// DexScanner - Binance DEX orderbook scanner
type DexScanner struct {
	ctx  	*common.Context
	Books	common.OrderBooks
	Skip	common.StringSet
	BNBPriceInRuneDex	float64
}

// this type is used only in combination - Thorchain testnet + BEP-2 mainnet (DEX)
type TradingPair struct {
	BaseAssetSymbol  string        `json:"base_asset_symbol"`
	QuoteAssetSymbol string        `json:"quote_asset_symbol"`
	ListPrice        common.Fixed8 `json:"list_price"`
	TickSize         common.Fixed8 `json:"tick_size"`
	LotSize          common.Fixed8 `json:"lot_size"`
}

type MarketDepth struct {
	Bids   [][]string `json:"bids"` // "bids": [ [ "0.0024", "10" ] ]
	Asks   [][]string `json:"asks"` // "asks": [ [ "0.0024", "10" ] ]
	Height int64      `json:"height"`
}

// NewDexScanner - initializes Binance DEX scanner
func NewDexScanner(ctx *common.Context) (*DexScanner, error) {
	log.Print("NewDexScanner start")
	defer log.Print("NewDexScanner end")

	d := DexScanner{
		ctx:	ctx,
		Books:	make(common.OrderBooks, len(ctx.Assets)),
		Skip:	make(common.StringSet, len(ctx.Assets)),
	}
	if ctx.TokenMapping {
		err := d.createTokenMap()
		if err != nil {
			return nil, err
		}
	}
	return &d, nil
}

// GetOrderbooks - reads binance DEX orderbooks
func (d *DexScanner) GetOrderbooks() (bool, error) {
	log.Print("GetOrderbooks start")
	defer log.Print("GetOrderbooks end")

	// if all assets had error and are now in skipped set delete the skipped set and check them again
	allSkipped := true
	for _, asset := range d.ctx.Assets {
		if _, skip := d.Skip[asset]; !skip {
			allSkipped = false
			break
		}
	}
	if allSkipped {
		d.Skip = make(common.StringSet, len(d.ctx.Assets))
	}
	someOK := false
	changed := false
	for _, asset := range d.ctx.Assets {
		if _, skip := d.Skip[asset]; skip {
			continue
		}
		if !d.ctx.AssetSync[asset].CanWorkOn(2) {
			someOK = true
			log.Printf("GetOrderbooks - SyncLock - asset %v skipped", asset)
			continue
		}
		func() {
			defer d.ctx.AssetSync[asset].WorkDone()
			depth, reversed, err := d.GetAssetDepth(string(asset))
			if err != nil {
				d.Skip[asset] = common.Void{} // this skip / ban should be active just for a defined time interval
				log.Printf("GetOrderbooks - GetDepth call returned error for %v. Error: %v", asset, err)
				return //continue
			}
			someOK = true
			log.Printf("Asset %v - start orderbook read\n", asset)
			ob := common.OrderBook{}
			if !reversed {
				ob.Asks = make(common.PriceVol, len(depth.Asks))
				ob.Bids = make(common.PriceVol, len(depth.Bids))
				for _, ask := range depth.Asks {
					price := common.N64x(ask[0])
					vol := common.N64x(ask[1])
					ob.Asks[price] = vol
					//log.Printf(" ... inserted ASK [%v, %v]\n", price, vol)
				}

				for _, bid := range depth.Bids {
					price := common.N64x(bid[0])
					vol := common.N64x(bid[1])
					ob.Bids[price] = vol
					//log.Printf(" ... inserted BID [%v, %v]\n", price, vol)
				}
			} else {
				ob.Asks = make(common.PriceVol, len(depth.Bids))
				ob.Bids = make(common.PriceVol, len(depth.Asks))
				for _, ask := range depth.Bids {
					p := common.Fx8ToFl64(common.N64x(ask[0]))
					v := common.Fx8ToFl64(common.N64x(ask[1]))
					price := common.Fl64ToFx8(1 / p)
					vol := common.Fl64ToFx8(p * v)
					ob.Asks[price] = vol
					//log.Printf(" ... inserted ASK [%v, %v]\n", price, vol)
				}

				for _, bid := range depth.Asks {
					p := common.Fx8ToFl64(common.N64x(bid[0]))
					v := common.Fx8ToFl64(common.N64x(bid[1]))
					price := common.Fl64ToFx8(1 / p)
					vol := common.Fl64ToFx8(p * v)
					ob.Bids[price] = vol
					//log.Printf(" ... inserted BID [%v, %v]\n", price, vol)
				}
			}

			if !d.Books[asset].IsEqual(&ob) {
				log.Printf("GetOrderbooks -  ... OB different than old OB - inserting (asset: %v)", asset)
				d.resum(&ob)
				d.Books[asset] = ob
				if asset == d.ctx.BNB {
					var bp, ap float64
					bp = 0; ap = 0
					if len(ob.AskSum) > 0 {
						ap = common.Fx8ToFl64(ob.AskSum[0][0])
					}
					if len(ob.BidSum) > 0 {
						bp = common.Fx8ToFl64(ob.BidSum[0][0])
					}
					p := 2 / (ap + bp) // BNB price in RUNE = 1 / (RUNE price in BNB)
					d.BNBPriceInRuneDex = p
				}
				log.Printf("GetOrderbooks - Asset %v orderbook read. Asks[%v] / Bids[%v].", asset, len(depth.Asks), len(depth.Bids))
				changed = true
			} else {
				log.Printf("GetOrderbooks -  ... OB equal with old OB - NOT inserting")
			}
		}()
	}
	if !someOK {
		return false, fmt.Errorf("GetOrderbooks - Something went wrong. All orderbook read failed. See previous log errors")
	}
	return changed, nil
}

func (d *DexScanner) resum(ob *common.OrderBook) {
	log.Print("resum start")
	defer log.Print("resum end")

	//log.Printf(" ... insering Bid Sums")
	d.resumBA(&ob.BidSum, &ob.Bids, true)
	//log.Printf(" ... insering Ask Sums")
	d.resumBA(&ob.AskSum, &ob.Asks, false)
}

func (d *DexScanner) resumBA(sums *[]common.BidAskSum, bas *common.PriceVol, bids bool) {
	log.Print("resumBA start")
	defer log.Print("resumBA end")

	*sums = make([]common.BidAskSum, len(*bas))
	i := 0
	for price, vol := range *bas {
		(*sums)[i] = common.BidAskSum{price, vol, 0}
		i++
		//log.Printf(" ... inserted BID/ASK [%v, %v]\n", price, vol)
	}
	if bids {
		sort.SliceStable(*sums, func(i, j int) bool {
			return (*sums)[i][0] > (*sums)[j][0]
		})
	} else {
		sort.SliceStable(*sums, func(i, j int) bool {
			return (*sums)[i][0] < (*sums)[j][0]
		})
	}
	prevSum := common.Fixed8(0)
	for i, v := range *sums {
		(*sums)[i][2] = prevSum + v[1]
		prevSum = (*sums)[i][2]
		//log.Printf(" ... BID sum for price [%v, %v]\n", (*sums)[i][0], (*sums)[i][2])
	}
}

func (d *DexScanner) GetAssetDepth(asset string) (*MarketDepth, bool, error) {
	log.Printf("GetAssetDepth(%v) start", asset)
	defer log.Printf("GetAssetDepth(%v) end", asset)

	if d.ctx.TokenMapping {
		if masset, ok := d.ctx.MapAssets[asset]; ok {
			asset = masset
		} else {
			return nil, false, fmt.Errorf("TokenMapping failed for this asset [%v]. Asset ignored.", asset)
		}
	}

	s := strings.Split(asset, ".")
	if len(s) != 2 {
		return nil, false, fmt.Errorf("Cannot split asset name with separator [.]")
	}
	md, err := d.GetDepth(s[1] + "_" + s[0])
	if err != nil {
		return nil, false, err
	}
	rev := s[1] == "BNB"
	return md, rev, nil
}

func (d *DexScanner) GetDepth(asset string) (*MarketDepth, error) {
	log.Printf("GetDepth(%v) start", asset)
	defer log.Printf("GetDepth(%v) end", asset)

	url := "https://" + d.ctx.PriceDexURL + "/api/v1/depth?symbol=" + asset
	log.Printf("GetDepth - calling %v", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("GetDepth - http.NewRequest error: %v", err)
		return nil, err
	}
	bytes, err := common.DoHttpRequest(req)
	if err != nil {
		log.Printf("GetDepth - common.DoHttpRequest error: %v", err)
		return nil, err
	}
	var depth MarketDepth
	err = json.Unmarshal(bytes, &depth)
	if err != nil {
		log.Printf("GetDepth - json.Unmarshal error: %v", err)
		return nil, err
	}
	return &depth, nil
}

// this 2 functions is used only in combination - Thorchain testnet + BEP-2 mainnet (DEX)
func (d *DexScanner) createTokenMap() error {
	log.Print("createTokenMap createTokenMap")
	defer log.Print("createTokenMap end")

	listOfPairs, err := d.getMarkets()
	if err != nil {
		return err
	}
	d.ctx.MapAssets = make(map[string]string, len(d.ctx.Assets) + 1)
	d.ctx.MapAssets[d.ctx.BNB] = "BNB.RUNE-B1A"
	d.ctx.MapAssets["BNB.TUSDB-000"] = "BUSD-BD1.BNB"

	m := make(map[string]string, len(listOfPairs))
	for _, tp := range listOfPairs {
		key := tp.QuoteAssetSymbol + "." + tp.BaseAssetSymbol
		val := key
		s := strings.Split(tp.BaseAssetSymbol, "-")
		if len(s) > 2 {
			log.Printf("createTokenMap [%15v] ERROR. Cannot split token symbol %v into two parts with separator [-] (comma) (for %v)", tp.BaseAssetSymbol, tp.BaseAssetSymbol, tp.QuoteAssetSymbol+"."+tp.BaseAssetSymbol)
			continue
		} else if len(s) == 2 {
			key = tp.QuoteAssetSymbol + "." + s[0]
		}
		log.Printf("createTokenMap Token mapping: m[%v] = %v", key, val)
		m[key] = val // eg m[BNB.BOLT] = BNB.BOLT-E42
	}
	for _, a := range d.ctx.Assets {
		s := strings.Split(a, ".")
		if len(s) != 2 {
			log.Printf("createTokenMap [%15v] ERROR. Cannot split into two parts with separator [.] (dot)", a)
			continue
		}
		base := s[0]
		long := s[1]
		short := long
		s = strings.Split(long, "-")
		if len(s) == 2 {
			short = s[0]
		}
		search := base + "." + short
		if sym, ok := m[search]; ok {
			d.ctx.MapAssets[a] = sym
			log.Printf("createTokenMap [%15v] succesfully inserted into map. map[%v]=%v", a, a, sym)
		} else {
			log.Printf("createTokenMap [%15v] ERROR. Cannot find corresponding asset on Mainnet (short name %v)", a, search)
			continue
		}
	}
	return nil
}

func (d *DexScanner) getMarkets() ([]TradingPair, error) {
	log.Print("getMarkets start")
	defer log.Print("getMarkets end")

	url := "https://" + d.ctx.PriceDexURL + "/api/v1/markets?limit=1000"
	log.Printf("getMarkets - calling %v", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("getMarkets - http.NewRequest error: %v", err)
		return nil, err
	}
	bytes, err := common.DoHttpRequest(req)
	if err != nil {
		log.Printf("getMarkets - common.DoHttpRequest error: %v", err)
		return nil, err
	}
	var data []TradingPair
	err = json.Unmarshal(bytes, &data)
	if err != nil {
		log.Printf("getMarkets - json.Unmarshal error: %v", err)
		return nil, err
	}
	return data, nil
}

/*
func (d *DexScanner) onMarketDelta(event *websocket.MarketDeltaEvent) {
	d.ctx.Log.Printf("Market delta event for %v\n", event.Symbol)
	s := strings.Split(event.Symbol, "_")
	if len(s) != 2 {
		d.ctx.Log.Printf("Market delta event prolem for %v - has not split by '_' to 2 parts\n", event.Symbol)
		return
	}
	sym := s[0] + "." + s[1]
	ob, ok := d.Books[sym]
	if !ok {
		d.ctx.Log.Printf("No books for %v but there should be\n", sym)
		return
	}
	if len(event.Bids) > 0 {
		for _, b := range event.Bids {
			p := b[0]
			v := b[1]
			if v > 0 { // adding to orderbook
				ob.Bids[p] = v
				d.ctx.Log.Printf(" ...adding to Bids [%v, %v]\n", p, v)
			} else {
				delete(ob.Bids, p)
				d.ctx.Log.Printf(" ...deleting from Bids [%v, %v]\n", p, v)
			}
		}
		d.reSumBids(sym)
	}
	if len(event.Asks) > 0 {
		for _, b := range event.Asks {
			p := b[0]
			v := b[1]
			if v > 0 { // adding to orderbooks
				ob.Asks[p] = v
				d.ctx.Log.Printf(" ...adding to Asks [%v, %v]\n", p, v)
			} else {
				delete(ob.Asks, p)
				d.ctx.Log.Printf(" ...deleting from Asks [%v, %v]\n", p, v)
			}
		}
		d.reSumAsks(sym)
	}
}
*/
