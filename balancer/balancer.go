package balancer

import (
	"fmt"
	"log"

	"gitlab.com/zlyzol/bepbalancer/common"
	"gitlab.com/zlyzol/bepbalancer/dexarb"
	"gitlab.com/zlyzol/bepbalancer/dexscn"
	"gitlab.com/zlyzol/bepbalancer/poolscn"
)

// Balancer - balancer structure
type Balancer struct {
	ctx *common.Context
	ps  *poolscn.PoolScanner
	dex *dexscn.DexScanner
}

// NewBalancer - creates new Balancer
func NewBalancer(ctx *common.Context, ps *poolscn.PoolScanner, dex *dexscn.DexScanner) (*Balancer, error) {
	log.Print("NewBalancer start")
	defer log.Print("NewBalancer end")

	m := Balancer{
		ctx: ctx,
		ps:  ps,
		dex: dex,
	}
	return &m, nil
}

func (bal *Balancer) Balance() error {
	log.Print("Balance start")
	defer log.Print("Balance end")

	test := 0
	if test == 0 {
		bnbpool := bal.ps.Pools["BNB.BNB"]
		for asset, ob := range bal.dex.Books {
			go func(asset string, ob common.OrderBook) {
				for {
					if !bal.ctx.AssetSync[asset].CanWorkOn(1) {
						return
					}
					pool := bal.ps.Pools[asset]
					success, err := bal.checkPoolBalance(asset, bnbpool, pool, ob)
					bal.ctx.AssetSync[asset].WorkDone()
					// TODO this shouls update only the affected pool and orderbook (for asset)
					if err != nil || !success {
						if err != nil {
							log.Printf("checkPoolBalance Error. See previous logs. Error: %v", err)
						}
						log.Print("checkPoolBalance - balancing dint happen. Check previous logs")
						return
					}
					if bal.ctx.Config.BotMode != common.BalancerMode.Balance {
						bal.dex.GetOrderbooks()
					}
					bal.ps.GetPools(asset)
				}
			}(asset, ob)
		}
	} else if test == 1 {
		bnbpool := bal.ps.Pools["BNB.BNB"]
		for asset, ob := range bal.dex.Books {
			if asset != "BNB.TUSDB-000" {
				//continue
			}
			func(asset string, ob common.OrderBook) {
				for {
					if !bal.ctx.AssetSync[asset].CanWorkOn(1) {
						return
					}
					pool := bal.ps.Pools[asset]
					success, err := bal.checkPoolBalance(asset, bnbpool, pool, ob)
					bal.ctx.AssetSync[asset].WorkDone()
					// TODO this shouls update only the affected pool and orderbook (for asset)
					if err != nil || !success {
						if err != nil {
							log.Printf("checkPoolBalance Error. See previous logs. Error: %v", err)
						}
						log.Print("checkPoolBalance - balancing dint happen. Check previous logs")
						return
					}
					if bal.ctx.Config.BotMode != common.BalancerMode.Balance {
						bal.dex.GetOrderbooks()
					}
					bal.ps.GetPools(asset)
				}
			}(asset, ob)
		}
	} else if test == 2 {
				// TEST
		bp := poolscn.PoolData{
			AssetDepth: 1000,
			RuneDepth:  150000,
			RunePrice:  150,
			BasePrice:  1.0,
		}
		p := poolscn.PoolData{
			AssetDepth: 105000,
			RuneDepth:  95000,
			RunePrice:  0.9047619,
			BasePrice:  0.00666, //0.00603,
		}
		o := common.OrderBook{
			Bids: common.PriceVol{
				601000: 100000000,
				556000: 100000000,
				546000: 100000000,
			},
			BidSum: []common.BidAskSum{
				{602000, 300000000, 300000000},
				{656000, 100000000, 400000000},
				{546000, 900000000, 1300000000},
			},
			Asks: common.PriceVol{
				666000: 100000000,
			},
			AskSum: []common.BidAskSum{
				{503000, 100000000, 100000000},
				{605000, 800000000, 900000000},
				{696000, 100000000, 300000000},
			},
		}

		log.Printf("Balancer TEST\n")
		bal.checkPoolBalance("TEST", bp, p, o)
		// TEST END
	}
	return nil
}

func (bal *Balancer) checkPoolBalance(asset string, bnbpool poolscn.PoolData, pool poolscn.PoolData, ob common.OrderBook) (bool, error) {
	log.Print("checkPoolBalance start")
	defer log.Print("checkPoolBalance end")

	if pool.BasePrice == 0 {
		return false, fmt.Errorf("checkPoolBalance - Balancer (for %v) Error - invalid pool. BasePrice = 0\n", asset)
	}
	var ba *[]common.BidAskSum
	var tradeType int8
	if len(ob.BidSum) > 0 && pool.BasePrice < common.Fx8ToFl64(ob.BidSum[0][0]) {
		// the pool price of asset is less than on DEX -> swap RUNE for asset
		ba = &ob.BidSum
		tradeType = common.OrderSide.SELL
	} else if len(ob.AskSum) > 0 && pool.BasePrice > common.Fx8ToFl64(ob.AskSum[0][0]) {
		// the pool price of asset is greater than on DEX -> (swap RUNE for BNB, if we dont have enough) buy asset on DEX and swap it for RUNE
		ba = &ob.AskSum
		tradeType = common.OrderSide.BUY
	} else {
		if asset == "BNB.BNB" {
			log.Printf("checkPoolBalance - No arb opportunity. %v highest bid = %s, pool price = %v, lowest ask = %s (for BNB reversed)", "RUNE", ob.BidSum[0][0], pool.BasePrice, ob.AskSum[0][0])
		} else if asset == "BNB.TUSDB-000" {
			log.Printf("checkPoolBalance - No arb opportunity. %v highest bid = %v, pool price = %v, lowest ask = %v (for BNB reversed)", asset, 1/common.Fx8ToFl64(ob.AskSum[0][0]), 1/pool.BasePrice, 1/common.Fx8ToFl64(ob.BidSum[0][0]))
		} else {
			log.Printf("checkPoolBalance - No arb opportunity. %v highest bid = %s, pool price = %v, lowest ask = %s (in BNB)", asset, ob.BidSum[0][0], pool.BasePrice, ob.AskSum[0][0])
		}
		return false, nil
	}
	if asset == "BNB.BNB" {
		log.Printf("checkPoolBalance - Maybe arb opportunity. %v highest bid = %s, pool price = %v, lowest ask = %s (for BNB reversed)", "RUNE", ob.BidSum[0][0], pool.BasePrice, ob.AskSum[0][0])
	} else if asset == "BNB.TUSDB-000" {
		log.Printf("checkPoolBalance - Maybe arb opportunity. %v highest bid = %v, pool price = %v, lowest ask = %v (for BNB reversed)", asset, 1/common.Fx8ToFl64(ob.AskSum[0][0]), 1/pool.BasePrice, 1/common.Fx8ToFl64(ob.BidSum[0][0]))
	} else {
		log.Printf("checkPoolBalance - Maybe arb opportunity. %v highest bid = %s, pool price = %v, lowest ask = %s (in BNB)", asset, ob.BidSum[0][0], pool.BasePrice, ob.AskSum[0][0])
	}

	// let minRune = min(bnb.rune/bolt.rune,1) (this is to factor in liquidity in the bnb pool)
	// BNB trade size is calculated by  (d * m * R * X)/(5* Y) or (% difference * minRune * bolt.rune * bnb.bnb)/(5 * bnb.rune) (depths)

	bidSize := common.Fx8ToFl64((*ba)[0][1]) * common.Fx8ToFl64((*ba)[0][0])  // first (highest BID/lowest ASK) size (in BNB)
	bidPrice := common.Fx8ToFl64((*ba)[0][0]) // first (highest BID/lowest ASK) price

	log.Printf("checkPoolBalance - Balancer using first bid/ask - [size : price] = [%.8f : %.8f]", bidSize, bidPrice)

	R := common.Fx8ToFl64(pool.RuneDepth)
	X := common.Fx8ToFl64(bnbpool.AssetDepth)
	Y := common.Fx8ToFl64(bnbpool.RuneDepth)
	m := 1.0
	m1 := Y / R
	if m1 < m {
		m = m1
	}
	d2 := pool.BasePrice
	for i := 1; ; i = i + 1 {
		d1 := bidPrice
		var d float64
		if d1 > d2 {
			d = (d1 - d2) / d1
		} else {
			d = (d2 - d1) / d2
		}

		// X/Y -  BNB price from pool
		BNBPriceInRune := bal.ps.BNBPriceInRune
		if bal.ctx.Config.BotMode == common.BalancerMode.Balance {
			// X/Y - use the lower price between BNB price from pool and from DEX, but use it just for balancer
			if bal.ps.BNBPriceInRune > bal.dex.BNBPriceInRuneDex {
				BNBPriceInRune = bal.dex.BNBPriceInRuneDex
			}
		}
		XY := 1/BNBPriceInRune
		//log.Printf("checkPoolBalance - Balancer (for %v) d = (d1 - d2) / d2 ... %v = (%v - %v)/%v\n", a, d, d1, d2, d2)
		// TODO - PROBLEM - ak rozdiel je velky napr 98% tak tradesize v bnb bude priliz, velky pre nakup na dexe za priliz nizku cenu, takze sa nedosiahne spravny balance
		// treba to premysliet .....
		tradeSize := XY * d * m * R / 5
		log.Printf("checkPoolBalance - Balancer (for %v) tradeSize %f = (%f * %v * %f * %f) / (5 * %f)", asset, tradeSize, d, m, R, X, Y)
		if tradeSize < bal.ctx.Config.BlancingLimit { // imbalance size is smaller than the limit in BNB -> we dont want to balance
			log.Printf("checkPoolBalance - Imbalance is small. Trade size (%v BNB) is less then the config limit (%v BNB) for balancing in pool %v", tradeSize, bal.ctx.Config.BlancingLimit, asset)
			return false, nil
		}
		log.Printf("checkPoolBalance - Balancer (for %v) tradeSize %f = (%f * %v * %f * %f) / (5 * %f)", asset, tradeSize, d, m, R, X, Y)
		if bidSize >= tradeSize { // we can do it - Thorchain price is lower, highest BID is enought for the trade size
			return bal.balancePool(asset, tradeSize, common.Fx8ToFl64((*ba)[i-1][0]), bidPrice, pool.BasePrice, tradeType, BNBPriceInRune) // swap RUNE or BNB for asset and sell on DEX
		} else {
			if i < len(*ba) && func() bool {
				if tradeType == common.OrderSide.BUY {
					return pool.BasePrice > common.Fx8ToFl64((*ba)[i][0])
				}
				return pool.BasePrice < common.Fx8ToFl64((*ba)[i][0])
			}() { // take into account next BID
				bidPrice2 := common.Fx8ToFl64((*ba)[i][0]) // first (highest) BID price
				bidSize2 := common.Fx8ToFl64((*ba)[i][1]) * common.Fx8ToFl64((*ba)[i][0]) // first (highest) BID size
				log.Printf("checkPoolBalance - Balancer using next(%v-th) bid/ask - [size : price] = [%f : %f]", i+1, bidSize2, bidPrice2)
				if tradeSize < bidSize+bidSize2 { // tradeSize is less, we take just a part of next bid size
					bidSize2 = tradeSize - bidSize
				}
				bidPrice = (bidPrice*bidSize + bidPrice2*bidSize2) / (bidSize + bidSize2)
				bidSize = bidSize + bidSize2
				log.Printf("checkPoolBalance - Balancer using next(%v-th) bid/ask - recalculated [size : price] = [%f : %f]", i+1, bidSize, bidPrice)
			} else { // no more BIDs
				tradeSize = bidSize
				return bal.balancePool(asset, tradeSize, common.Fx8ToFl64((*ba)[i-1][0]), bidPrice, pool.BasePrice, tradeType, BNBPriceInRune) // swap RUNE or BNB for asset and sell on DEX
			}
		}
	}
}

func (bal *Balancer) balancePool(asset string, tradeSize float64, limitPrice float64, supAvgDexPrice float64, poolPrice float64, tradeType int8, bnbPriceInRune float64) (bool, error) {
	log.Print("balancePool start")
	defer log.Print("balancePool end")

	arb, err := dexarb.NewDexArb(*bal.ctx, asset, tradeSize, limitPrice, supAvgDexPrice, poolPrice, tradeType, bnbPriceInRune)
	if err != nil {
		return false, err
	}
	success := arb.Arbitrage()

	return success, nil
}
