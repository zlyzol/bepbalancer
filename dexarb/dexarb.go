package dexarb

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"math"
	"time"

	"github.com/binance-chain/go-sdk/client/transaction"
	"github.com/binance-chain/go-sdk/client/websocket"
	types "github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/types/msg"

	"gitlab.com/zlyzol/bepbalancer/common"
)

type Transaction struct {
	BlockHeight   int64  `json:"blockHeight,omitempty"` // block height
	Code          int64  `json:"code,omitempty"`        // transaction result code	0
	ConfirmBlocks int64  `json:"confirmBlocks,omitempty"`
	Data          string `json:"data,omitempty"`
	FromAddr      string `json:"fromAddr,omitempty"`  // from address
	OrderId       string `json:"orderId,omitempty"`   // order ID
	TimeStamp     string `json:"timeStamp,omitempty"` // time of transaction
	ToAddr        string `json:"toAddr,omitempty"`    // to address
	TxAge         int64  `json:"txAge,omitempty"`
	TxAsset       string `json:"txAsset,omitempty"`
	TxFee         string `json:"txFee,omitempty"`
	TxHash        string `json:"txHash,omitempty"` // hash of transaction
	TxType        string `json:"txType,omitempty"` // type of transaction
	Value         string `json:"value,omitempty"`  // value of transaction
	Source        int64  `json:"source,omitempty"`
	Sequence      int64  `json:"sequence,omitempty"`
	SwapId        string `json:"swapId,omitempty"` // Optional. Available when the transaction type is one of HTL_TRANSFER, CLAIM_HTL, REFUND_HTL, DEPOSIT_HTL
	ProposalId    string `json:"proposalId,omitempty"`
	Memo          string `json:"memo,omitempty"`
}

type Transactions struct {
	Total int64         `json:"total"` // "total": 2
	Tx    []Transaction `json:"tx"`
}

// TokenBalance structure
type TokenBalance struct {
	Symbol string        `json:"symbol"`
	Free   common.Fixed8 `json:"free"`
	Locked common.Fixed8 `json:"locked"`
	Frozen common.Fixed8 `json:"frozen"`
}

// BalanceAccount definition
type BalanceAccount struct {
	Number    int64          `json:"account_number"`
	Address   string         `json:"address"`
	Balances  []TokenBalance `json:"balances"`
	PublicKey []uint8        `json:"public_key"`
	Sequence  int64          `json:"sequence"`
	Flags     uint64         `json:"flags"`
}

// DexArb - Binance DEX Arbitrage structure
type DexArb struct {
	ctx common.Context

	// Arbitrage properties
	Asset				string
	TradeType			int8    // OrderSide.BUY, OrderSide.SELL
	LimitPrice			float64 // in BNB
	TradeSize			float64 // in BNB - total BNB we should balance/arb
	SupAvgPrice			float64 // in BNB - supposed average price of the Asset
	PoolPrice			float64 // in BNB - pool price of Asset
	BNBPriceInRune		float64 // BNB price in RUNE calculated from pools
	BNBPriceInRuneDex	float64 // BNB price in RUNE calculated from DEX (RUNE/BNB pair)

	TradeSizeAsset float64 // total Aset we should balance/arb
	TradeSizeRune  float64 // -""- in RUNE

	// account balance
	bnb     float64
	rune    float64
	asbal   float64 // asset balance
	orderid string
}

// NewArbTrade - initializes Arbitrage Trade
func NewDexArb(ctx common.Context, asset string, tradeSize float64, limitPrice float64, supAvgPrice float64, poolPrice float64, tradeType int8, bnbPriceInRune float64) (*DexArb, error) {
	a := DexArb{
		ctx:            ctx,
		Asset:          asset,
		TradeType:      tradeType,
		LimitPrice:     limitPrice,  // in BNB
		TradeSize:      tradeSize,   // in BNB
		SupAvgPrice:    supAvgPrice, // in BNB
		PoolPrice:		poolPrice,	 // in BNB
		BNBPriceInRune: bnbPriceInRune,
		TradeSizeAsset: tradeSize / poolPrice,
		TradeSizeRune:  tradeSize * bnbPriceInRune,
	}
	return &a, nil
}

// Arbitrage executes the pool balancing / arbitrage trade
func (a *DexArb) Arbitrage() bool {
	log.Print("Arbitrage start")
	defer log.Print("Arbitrage end")

	err := a.getAccBal()
	if err != nil {
		log.Printf("ERROR: Cannot get account balance. Error: %v", err)
		return false
	}
	log.Printf("We have %v RUNE, %v BNB, %v %v", a.rune, a.bnb, a.Asset, a.asbal)
	oldrune := a.rune
	if a.TradeType == common.OrderSide.SELL { // swap RUNE (and BNB if needed) for asset and sell asset on DEX
		if !a.swapRune() {
			return false
		}
		if a.ctx.Config.BotMode == common.BalancerMode.Arbitrage {
			if !a.sellAsset() {
				return false
			}
		}
	} else if a.TradeType == common.OrderSide.BUY { //// (swap RUNE for BNB if needed) than buy asset on DEX and swap it back for RUNE?
		if a.ctx.Config.BotMode == common.BalancerMode.Arbitrage {
			if !a.buyAsset() {
				return false
			}
		}
		if !a.swapAsset() {
			return false
		}
	}
	a.getAccBal()
	log.Printf("SUCCESS - arbitrage.go ArbTrade.start() - arbitrage ended, we had %v RUNE and now we have %v RUNE\n", oldrune, a.rune)
	return true
}

func (a *DexArb) swapRune() bool {
	log.Print("swapRune start")
	defer log.Print("swapRune end")

	if a.Asset == a.ctx.BNB {
		return a.swapBNBInBNBPool()
	} else {
		return a.swapRuneOrig()
	}
}

func (a *DexArb) swapBNBInBNBPool() bool {
	log.Print("swapBNBInBNBPool start")
	defer log.Print("swapBNBInBNBPool end")

	tradeSize := math.Min(a.TradeSizeRune, a.bnb-a.ctx.Config.Reserve)
	if a.bnb-a.ctx.Config.Reserve < a.TradeSizeRune {
		log.Printf("WARNING - not enough %v for swapping the full calculated trade size. We have %v, we need %v", a.Asset, a.bnb, a.TradeSizeAsset)
	}
	return a.swap(a.ctx.BNB, a.ctx.RUNE, tradeSize, 0)
}

func (a *DexArb) swapRuneOrig() bool {
	log.Print("swapRuneOrig start")
	defer log.Print("swapRuneOrig end")

	var mux sync.Mutex
	success := true
	var wg sync.WaitGroup
	if a.rune >= a.TradeSizeRune { // we have enough RUNE to swap for asset
		wg.Add(1)
		go func() {
			defer wg.Done()
			b := a.swap(a.ctx.RUNE, a.Asset, a.TradeSizeRune, 0)
			mux.Lock()
			success = success && b
			mux.Unlock()
		}()
	} else { // we dont have enough RUNE to swap for asset -> we swap all RUNE and some/all BNB
		wg.Add(1)
		go func() {
			defer wg.Done()
			b := a.swap(a.ctx.RUNE, a.Asset, a.rune, a.LimitPrice*a.BNBPriceInRune) // limit is just to be safe not to buy too expensive asset - LIMIT = a.LimitPrice
			mux.Lock()
			success = success && b
			mux.Unlock()
		}()
		bnbToSwap := a.TradeSize - a.rune/a.BNBPriceInRune
		if bnbToSwap >= a.bnb+a.ctx.Config.Reserve { // BNB reserved for fees
			bnbToSwap = bnbToSwap - a.bnb - a.ctx.Config.Reserve
		}
		wg.Add(1)
		go func() {
			defer wg.Done()
			b := a.swap(a.ctx.BNB, a.Asset, bnbToSwap, a.LimitPrice) // limit is just to be safe not to buy too expensive asset - LIMIT = a.LimitPrice
			mux.Lock()
			success = success && b
			mux.Unlock()
		}()
	}
	wg.Wait()
	return success
}

func (a *DexArb) swapAsset() bool {
	log.Print("swapAsset start")
	defer log.Print("swapAsset end")

	if a.Asset == a.ctx.BNB {
		return a.swapRuneInBNBPool()
	} else {
		return a.swapAssetOrig()
	}
}

func (a *DexArb) swapRuneInBNBPool() bool {
	log.Print("swapAssetOrig start")
	defer log.Print("swapAssetOrig end")

	toSwap := math.Min(a.rune, a.TradeSize*a.BNBPriceInRune)
	if a.rune < toSwap {
		log.Printf("WARNING - not enough RUNE for swapping the full calculated trade size. We have %v, we need %v", a.rune, toSwap)
	}
	if toSwap < a.ctx.Config.BlancingLimit {
		log.Printf("WARNING - the value of available RUNE (in BNB price) is too small for trading (amount = %v, price in BNB = %v).", toSwap, toSwap/a.BNBPriceInRune)
		return false
	}
	return a.swap(a.ctx.RUNE, a.Asset, toSwap, 0)
}

func (a *DexArb) swapAssetOrig() bool {
	log.Print("swapAssetOrig start")
	defer log.Print("swapAssetOrig end")

	if a.asbal < a.TradeSizeAsset {
		log.Printf("WARNING - not enough %v for swapping the full calculated trade size. We have %v, we need %v", a.Asset, a.asbal, a.TradeSizeAsset)
	}
	toSwap := math.Min(a.asbal, a.TradeSizeAsset)
	if toSwap * a.PoolPrice < a.ctx.Config.BlancingLimit {
		log.Printf("WARNING - the value of available asset %v in BNB is too small for trading (amount = %v, price in BNB = %v).", a.Asset, toSwap, toSwap * a.PoolPrice)
		return false
	}
	return a.swap(a.Asset, a.ctx.RUNE, toSwap, 0)
}

func (a *DexArb) SwapWrapper(what, forWhat string, amount, limit float64) bool {
	return a.swap(what, forWhat, amount, limit)
}

func (a *DexArb) swap(what, forWhat string, amount, limit float64) bool {
	log.Print("swap start")
	defer log.Print("swap end")
	s := strings.Split(what, ".")
	if len(s) == 2 {
		what = s[1]
	}
	s = strings.Split(forWhat, ".")
	if len(s) == 2 {
		forWhat = s[1]
	}
	m := "SWAP:" + forWhat
	if limit > 0 {
		m = m + "::LIM:" + common.Fl64ToFx8(limit).String()
	}
	millis := time.Now().UnixNano() / 1000000
	msgs := []msg.Transfer{{
		ToAddr: a.ctx.ThorBNBAccAddr,
		Coins:  types.Coins{types.Coin{Denom: what, Amount: common.Fl64ToFxI64(amount)}},
	}}
	log.Printf("swap - MSGS: [%+v]", msgs)

	txres, err := a.ctx.Dex.SendToken(msgs, true, transaction.WithMemo(m))
	if err != nil {
		balance, err := a.getAccount()
		if err != nil {
			log.Printf("ERROR: swap - getAccount call failed. Error: [%v]", err)
			return false
		}
		txres, err = a.ctx.Dex.SendToken(msgs, true, transaction.WithMemo(m), transaction.WithAcNumAndSequence(balance.Number, balance.Sequence+1))
		if err != nil {
			log.Printf("ERROR: swap - Cannot swap %s for %s for trade (memo:[%s]). a.ctx.Dex.SendToken error: [%v]", what, forWhat, m, err)
			return false
		}
	}
	err = a.getSwapRes(strings.ToUpper(txres.Hash), millis)
	if err != nil {
		log.Printf("ERROR: swap - Cannot swap %s for %s for trade (memo:[%s]). getSwapRes error: [%v]", what, forWhat, m, err)
		return false
	}
	log.Printf("Swap mission ended (%s for %s (memo:[%s])", what, forWhat, m)
	return true
}

func (a *DexArb) getSwapRes(txHash string, fromMillis int64) error {
	log.Print("getSwapRes start")
	defer log.Print("getSwapRes end")

	ch := make(chan struct{}, 1)  // account event receiver channel
	chQ := make(chan struct{}, 3) // quit channel for SubscribeAccountEvent
	err := a.ctx.Dex.SubscribeAccountEvent(a.ctx.TradingWallet, chQ, func(event *websocket.AccountEvent) {
		ch <- struct{}{}
	}, nil, nil)
	if err != nil {
		return err
	}
	defer func() {
		chQ <- struct{}{}
		chQ <- struct{}{}
		chQ <- struct{}{}
	}()
	timeout := time.After(3 * time.Minute)
	ticker := time.NewTicker(4 * time.Second)
	for {
		select {
		case <-ch:
			log.Print("Swap result check on account event")
			time.Sleep(100 * time.Millisecond)
			ok, err := a.checkSwapRes(txHash, &fromMillis)
			if err != nil {
				return err
			}
			if ok {
				log.Print("Swap result confirmed on account event")
				return nil
			}
		case <-ticker.C:
			log.Print("Swap result check ticker")
			ok, err := a.checkSwapRes(txHash, &fromMillis)
			if err != nil {
				return err
			}
			if ok {
				log.Print("Swap result confirmed on ticker")
				return nil
			}
		case <-timeout:
			log.Print("Swap result check on timeout")
			ok, err := a.checkSwapRes(txHash, &fromMillis)
			if err != nil {
				return err
			}
			if ok {
				log.Print("Swap result confirmed on timeout")
				return nil
			}
			return fmt.Errorf("getSwapRes() - TIMEOUT ERROR - it seems NOT to be swapped after %v sec", 10)
		}
	}
}

func (a *DexArb) checkSwapRes(txHash string, fromMillis *int64) (bool, error) {
	log.Print("checkSwapRes start")
	defer log.Print("checkSwapRes end")
	// searching for outbond:TxHash
	// https://testnet-dex.binance.org/api/v1/transactions?address=tbnb13egw96d95lldrhwu56dttrpn2fth6cs0axzaad&blockHeight=76990710&startTime=1585724423000
	query := fmt.Sprintf("?address=%v&startTime=%d&side=RECEIVE", a.ctx.TradingWallet, *fromMillis)
	txs, err := a.GetTransactions(query)
	if err != nil {
		return false, err
	}
	searchForOk := "OUTBOUND:" + txHash
	searchForErr := "REFUND:" + txHash
	for _, tx := range txs.Tx {
		m := strings.ToUpper(tx.Memo)
		if strings.Index(m, searchForOk) != -1 {
			log.Printf("Swap confirmation detected [%v]", m)
			return true, nil
		}
		m = strings.ToUpper(tx.Memo)
		if strings.Index(m, searchForErr) != -1 {
			log.Printf("Swap rejection detected [%v]", m)
			return false, fmt.Errorf("Swap rejected [%v]", m)
		}
		//2020-04-14T06:34:03.276Z
		t, err := time.Parse(time.RFC3339, tx.TimeStamp)
		if err != nil {
			log.Printf("Cannot parse time from Tx: %v", tx.TimeStamp)
			return false, err
		}
		t2 := t.UnixNano() / 1000000
		if *fromMillis < t2 {
			*fromMillis = t2 + 1 // +1 millisecond not to repeatedly read the same Tx
		}
	}
	// OUTBOUND Tx not found yet
	return false, nil
}

func (a *DexArb) GetTransactions(query string) (*Transactions, error) {
	log.Print("GetTransactions start")
	defer log.Print("GetTransactions end")

	url := "https://" + a.ctx.DexURL + "/api/v1/transactions" + query
	log.Printf("GetTransactions - calling %v", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("GetTransactions - http.NewRequest error: %v", err)
		return nil, err
	}
	bytes, err := common.DoHttpRequest(req)
	if err != nil {
		log.Printf("GetTransactions - common.DoHttpRequest error: %v", err)
		return nil, err
	}
	var res Transactions
	err = json.Unmarshal(bytes, &res)
	if err != nil {
		log.Printf("GetTransactions - json.Unmarshal error: %v", err)
		return nil, err
	}
	return &res, nil
}

func (a *DexArb) sellAsset() bool { // sell asset on DEX
	log.Print("sellAsset start")
	defer log.Print("sellAsset end")

	err := a.getAccBal()
	if err != nil {
		log.Printf("ERROR: Cannot get account balance. Error: %v", err)
		return false
	}
	amount := math.Min(a.TradeSize, a.asbal)
	a.orderid = ""
	// TODO - IOC order
	ores, err := a.ctx.Dex.CreateOrder(a.Asset, "BNB", a.TradeType, common.Fl64ToFxI64(a.LimitPrice), common.Fl64ToFxI64(amount), true)
	if err != nil {
		log.Printf("ERROR: sellAsset - Cannot make the trade on DEX - Create order error: [%v]", err)
		return false
	}
	a.orderid = ores.OrderId
	return a.getOrderOutcome()
}

func (a *DexArb) buyAsset() bool { // buy asset on DEX (swap RUNE for BNB if needed)
	log.Print("buyAsset start")
	defer log.Print("buyAsset end")

	err := a.getAccBal()
	if err != nil {
		log.Printf("ERROR: Cannot get account balance. Error: %v", err)
		return false
	}
	bnbAmount := a.TradeSize
	if a.bnb-a.ctx.Config.Reserve < a.TradeSize { // we have not enough BNB, so we need to swap
		runeToSwap := (a.TradeSize - a.bnb + a.ctx.Config.Reserve) * a.BNBPriceInRune
		if runeToSwap > a.rune { // we have not enough RUNE for the full trade, so we will swap what we have
			runeToSwap = a.rune
		}
		if !a.swap(a.ctx.RUNE, "BNB", runeToSwap, 0) {
			return false
		}
		err := a.getAccBal()
		if err != nil {
			log.Printf("ERROR: Cannot get account balance. Error: %v", err)
			return false
		}
		bnbAmount = math.Min(a.TradeSize, a.bnb-a.ctx.Config.Reserve)
	}
	asssetAmount := math.Min(bnbAmount, a.TradeSize*a.PoolPrice)
	a.orderid = ""
	// TODO - vieme zadat IOC order nejako?   ??????????
	ores, err := a.ctx.Dex.CreateOrder(a.Asset, "BNB", a.TradeType, common.Fl64ToFxI64(a.LimitPrice), common.Fl64ToFxI64(asssetAmount), true)
	if err != nil {
		log.Printf("ERROR: buyAsset - Cannot make the trade on DEX - a.ctx.Dex.CreateOrder error: [%v]", err)
		return false
	}
	a.orderid = ores.OrderId
	return a.getOrderOutcome()
}

func (a *DexArb) getOrderOutcome() bool {
	log.Print("getOrderOutcome start")
	defer log.Print("getOrderOutcome end")

	ticker := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-ticker.C:
			osr := a.getOrderStatusResut("")
			if osr == 1 {
				return true
			} else if osr == -1 {
				return false
			}
		case <-time.After(10 * time.Second):
			osr := a.getOrderStatusResut("")
			if osr == 1 {
				log.Printf("WARNING - dexarb.go getOrderOutcome() - TIMEOUT - it seems be bought but after 10 sec")
				return true
			}
			log.Printf("ERROR - dexarb.go getOrderOutcome() - TIMEOUT - it seems NOT to be bought after 10 sec")
			return false
		}
	}
}

func (a *DexArb) getOrderStatusResut(orderStatus string) int {
	log.Print("getOrderStatusResut start")
	defer log.Print("getOrderStatusResut end")

	// orderStatus // "Ack", "Canceled", "Expired", "IocNoFill", "PartialFill", "FullyFill", "FailedBlocking", "FailedMatching", "Unknown"
	if orderStatus == "" {
		o, err := a.ctx.Dex.GetOrder(a.orderid)
		if err != nil {
			log.Printf("ERROR: getOrderStatusResut - Cannot get order - GetOrder error: [%v]", err)
			return -1 // failed
		}
		orderStatus = o.Status
	}
	if orderStatus == "Ack" {
		return 0 // we do nothing, we wait
	}
	if orderStatus == "FullyFill" || orderStatus == "PartialFill" {
		if orderStatus == "PartialFill" {
			_, err := a.ctx.Dex.CancelOrder(a.Asset, "BNB", a.orderid, true)
			if err != nil {
				log.Printf("ERROR / WARNING: buyAsset - Cannot cancel (PartialFill-ed) order on DEX - CancelOrder error: [%v]\n", err)
			}
		}
		return 1 // success
	} else {
		log.Printf("ERROR: buyAsset - Cannot successfully execute buy order on DEX - OrderStatus: [%v]\n", orderStatus)
		return -1 // failed
	}
}

func (a *DexArb) getAccount() (*BalanceAccount, error) {
	log.Print("getAccount start")
	defer log.Print("getAccount end")

	url := "https://" + a.ctx.DexURL + "/api/v1/account/" + a.ctx.TradingWallet
	log.Printf("getAccBal url = %v\n", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	bytes, err := common.DoHttpRequest(req)
	if err != nil {
		return nil, err
	}
	var data BalanceAccount
	err = json.Unmarshal(bytes, &data)
	if err != nil {
		return nil, err
	}

	for _, b := range data.Balances {
		log.Printf("Accoun balance [%v: %v]", b.Symbol, b.Free)
	}
	return &data, nil
}

func (a *DexArb) getAccBal() error {
	log.Print("getAccBal start")
	defer log.Print("getAccBal end")

	balance, err := a.getAccount()
	if err != nil {
		return err
	}
	a.rune = -1.0
	a.bnb = -1.0
	a.asbal = -1.0
	for _, coin := range balance.Balances {
		symbol := a.ctx.CHAIN + "." + coin.Symbol
		if symbol == a.ctx.RUNE || symbol == a.ctx.BNB || symbol == a.Asset {
			floatAmount, err := strconv.ParseFloat(coin.Free.String(), 64)
			if err != nil {
				return fmt.Errorf("ERROR onNewOrder - cannot ParseFloat of amount [%v] of [%v]. Error: %v", coin.Free, symbol, err)
			}
			log.Printf("getAccBal - We have %v of [%v]\n", floatAmount, symbol)
			if symbol == a.ctx.RUNE {
				a.rune = floatAmount
			} else if symbol == a.ctx.BNB {
				a.bnb = floatAmount
			} else if symbol == a.Asset {
				a.asbal = floatAmount
			}
			if a.rune >= 0 && a.bnb >= 0 && a.asbal >= 0 {
				break
			}
		}
	}
	if a.rune < 0 {
		a.rune = 0
	}
	if a.bnb < 0 {
		a.bnb = 0
	}
	if a.asbal < 0 {
		a.asbal = 0
	}
	return nil
}
