package common

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"sort"
	"time"

	"github.com/binance-chain/go-sdk/client"
	types "github.com/binance-chain/go-sdk/common/types"
	"github.com/binance-chain/go-sdk/keys"
)

// Context - context data (testnet or mainnet etc)
type Context struct {
	Config		*Config
	//NetworkType int8 // TestNet, ChaosNet, MainNet

	TradingWallet string

	SeedURL     string
	MidgardURL  string
	ThorBNBAddr string
	ThorBNBAccAddr	types.AccAddress
	Assets      []string

	DexURL       string           // DEX url for balancer wallet
	PriceDexURL  string           // DEX url for price feed (if TokenMapping is on, then this is always mainnet DEX)
	Dex          client.DexClient // DEX client for balancer wallet and swaps
	TokenMapping bool             // token symbol mapping for Thorchain Testnet to map BEP-2 testnet tokens to BEP-2 mainnet tokens for price query on Binance DEX
	// this map is used only in combination - Thorchain testnet + BEP testnet for balancer wallet + BEP mainnet (DEX) for price feed
	MapAssets map[string]string // asset symbol mapping from testnet to mainnet, eg BNB.BOLT-E42 -> BNB.BOLT-4C6

	RUNE  string // RUNE token full name - "BNB.RUNE-A1F" for testnet or "BNB.RUNE-B1A" for mainnet
	BNB   string // BASE asset (BNB) token full name - "BNB.BNB" for testnet or "BNB.BNB" for mainnet
	CHAIN string // just BNB for now

	AssetSync	AssetSyncMap
}

// ThorchainEndpoint8080 defines model for ThorchainEndpoint.
type ThorchainEndpoint8080 struct {
	Address *string `json:"address,omitempty"`
	Chain   *string `json:"chain,omitempty"`
	PubKey  *string `json:"pub_key,omitempty"`
}

// ThorchainEndpoints8080 defines model for ThorchainEndpoints.
type ThorchainEndpoints8080 struct {
	Current *[]ThorchainEndpoint8080 `json:"current,omitempty"`
}

// ThorchainEndpoint1317 defines model for ThorchainEndpoint.
type ThorchainEndpoint1317 struct {
	BNB *[]string `json:"BNB,omitempty"`
}

// ThorchainEndpoints1317 defines model for ThorchainEndpoints.
type ThorchainEndpoints1317 struct {
	Chains *ThorchainEndpoint1317 `json:"chains,omitempty"`
}

func NewContext(configFile string) (*Context, error) {
	log.Print("NewContext start")
	defer log.Print("NewContext end")

	ctx := Context{
		BNB:         "BNB.BNB",
		CHAIN:       "BNB",
	}

	if configFile == "" {
		configFile = "config.json"
	}
	ctx.Config = LoadConfiguration(configFile)

	var dexCliNetwork types.ChainNetwork
	switch ctx.Config.NetworkType {
	case NetworkType.TestNet:
		ctx.SeedURL = TestNetSeedURL
		ctx.DexURL = TestNetDexURL
		ctx.PriceDexURL = TestNetDexURL
		ctx.RUNE = "BNB.RUNE-A1F"
		dexCliNetwork = types.TestNetwork
		types.Network = types.TestNetwork // We must set this global variable for the key manager to work properly !
	case NetworkType.ChaosNet:
		ctx.SeedURL = ChaosNetSeedURL
		ctx.DexURL = MainNetDexURL
		ctx.PriceDexURL = MainNetDexURL
		ctx.TokenMapping = false
		ctx.RUNE = "BNB.RUNE-B1A"
		dexCliNetwork = types.ProdNetwork
	case NetworkType.MainNet:
		ctx.SeedURL = MainNetSeedURL
		ctx.DexURL = MainNetDexURL
		ctx.PriceDexURL = MainNetDexURL
		ctx.TokenMapping = false
		ctx.RUNE = "BNB.RUNE-B1A"
		dexCliNetwork = types.ProdNetwork
	}
	// keys & address
	key, err := keys.NewKeyStoreKeyManager(ctx.Config.Wallet.Keystore, ctx.Config.Wallet.Password)
	if err != nil {
		log.Printf("Cannot open keystore file %v", err)
		return nil, err
	}
	ctx.TradingWallet = key.GetAddr().String()

	// create Binance DEX Client
	ctx.Dex, err = client.NewDexClient(ctx.DexURL, dexCliNetwork, key)
	if err != nil {
		log.Printf("Cannot create binance DexClient %v", err)
		return nil, err
	}
	if TOKEN_MAPPING_ON && ctx.Config.NetworkType == NetworkType.TestNet {
		ctx.TokenMapping = true
		ctx.PriceDexURL = MainNetDexURL
	}

	// get midgard api url
	err = ctx.getMidgard()
	if err != nil {
		return nil, err
	}
	log.Printf("Midgard API url: [%v]\n", ctx.MidgardURL)

	err = ctx.getPools()
	if err != nil {
		return nil, err
	}

	return &ctx, err
}

func (ctx *Context) getMidgard() error {
	log.Print("getMidgard start")
	defer log.Print("getMidgard end")

	type mapAddr struct {
		n  int
		ip string
	}
	type arrAddr struct {
		a  string
		n  int
		ip string
	}
	for try := 1; try < SEED_CONSENSUS_TRY_CNT; try++ {
		url := "https://" + ctx.SeedURL
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			return err
		}
		bytes, err := DoHttpRequest(req)
		if err != nil {
			return err
		}
		var nodes []string
		err = json.Unmarshal(bytes, &nodes)
		if err != nil {
			return err
		}

		if ctx.Config.DebugSeed != "" {
			nodes = []string{(ctx.Config.DebugSeed)}
		}

		if len(nodes) < 1 {
			return fmt.Errorf("Seed returned 0 nodes %s", url)
		}
		m := make(map[string]mapAddr)
		bad := 0
		minCnt := int(math.Ceil(float64(len(nodes))/3 + 1))
		if len(nodes) == 1 {
			minCnt = 1
		}
		log.Printf("required number of 1/3 + 1 nodes is %v", minCnt)

		// DEBUG
		for i, ip := range nodes {
			log.Printf("Thorchain node address %v: %v", i, ip)
		}

		for _, ip := range nodes {
			if ip == "3.231.145.198" || ip == "3.216.90.157" {
				continue
			}

			tep, err := ctx.getThorchainPoolAddress(ip)
			if err != nil {
				bad++
				log.Printf("failed call http://%v:1317/thorchain/vault/addresses - Error: %v", ip, err)
				continue
			}
			if *tep.Chains.BNB == nil || len(*tep.Chains.BNB) == 0 {
				log.Printf("failed to find BNB chain pools on server http://%v:1317/thorchain/vault/addresses", ip)
				continue
			}
			pa := (*tep.Chains.BNB)[0]
			if ma, ok := m[pa]; ok {
				m[pa] = mapAddr{n: ma.n + 1, ip: ma.ip} // use the first found IP
			} else {
				m[pa] = mapAddr{n: 1, ip: string(ip)}
			}
		}
		var p []arrAddr
		p = make([]arrAddr, len(m))
		for k, v := range m {
			i := sort.Search(len(p), func(i int) bool { return p[i].n < v.n })
			p = append(p, arrAddr{})
			copy(p[i+1:], p[i:])
			p[i] = arrAddr{a: k, n: v.n, ip: v.ip}
		}
		log.Printf("Found Thorchain BNB chain pool addresses: %v", p)

		if len(p) < 1 {
			return fmt.Errorf("NOT EVEN ONE Thorchain BNB chain pool found")
		}
		if p[0].n < minCnt {
			log.Printf("Error: less than 1/3 + 1 nodes agreed on BNB chain pool address - try again - try %v/10", try)
			time.Sleep(SEED_CONSENSUS_TRY_PAUSE_SEC * time.Second)
		} else {
			// found enough nodes which agree on BNB chain pool address
			ctx.MidgardURL = "http://" + p[0].ip + ":8080" // ths is the first returned server from seed that also agreed on valid Thorchain BNB pool wallet address
			ctx.ThorBNBAddr = p[0].a // Thorchain BNB pool wallet address
			ctx.ThorBNBAccAddr, err = types.AccAddressFromBech32(ctx.ThorBNBAddr)
			if err != nil {
				return err
			}
			return nil
		}
	}
	return fmt.Errorf("Error: less than 1/3 + 1 nodes agreed on BNB chain pool address (after 10x try)")
}

func (ctx *Context) getThorchainPoolAddress(ip string) (*ThorchainEndpoints1317, error) {
	log.Printf("getThorchainPoolAddress %v start", ip)
	defer log.Printf("getThorchainPoolAddress %v end", ip)

	//url := "http://" + ip + "/v1/thorchain/pool_addresses"
	url := "http://" + ip + ":1317/thorchain/vaults/addresses"
	
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	bytes, err := DoHttpRequest(req)
	if err != nil {
		return nil, err
	}
	var data ThorchainEndpoints1317
	err = json.Unmarshal(bytes, &data)
	if err != nil {
		return nil, err
	}
	return &data, nil
}

func (ctx *Context) getPools() error {
	log.Printf("getPools start")
	defer log.Printf("getPools end")

	url := ctx.MidgardURL + "/v1/pools"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	bytes, err := DoHttpRequest(req)
	if err != nil {
		return err
	}
	var pools []string
	err = json.Unmarshal(bytes, &pools)
	if err != nil {
		return err
	}
	ctx.Assets = make([]string, len(pools))
	ctx.AssetSync = make(AssetSyncMap, len(pools))
	for i, asset := range pools {
		ctx.Assets[i] = asset
		ctx.AssetSync[asset] = &AssetStatusSync{}
		log.Printf("Pooled asset: [%v]", ctx.Assets[i])
	}
	return nil
}

// http request executor
func DoHttpRequest(req *http.Request) ([]byte, error) {
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if 200 != resp.StatusCode {
		return nil, fmt.Errorf("%s", body)
	}
	return body, nil
}
