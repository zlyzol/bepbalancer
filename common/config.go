package common

import (
	"time"
	"os"
	"encoding/json"
)

// NetworkType - struct type enum
var NetworkType = struct {
	TestNet  int8
	ChaosNet int8
	MainNet  int8
}{1, 2, 3}

// BalancerMode - struct type enum
var BalancerMode = struct {
	Balance		int8
	Arbitrage	int8
}{1, 2}

const SEED_CONSENSUS_TRY_CNT = 10 // how many times we try to reach consensus on Thorchain pool BNB address (1/3 + 1 must agree on one address)
const SEED_CONSENSUS_TRY_PAUSE_SEC = 10 // pause between tries to reach consensus on Thorchain pool BNB address
const SEED_CONSENSUS_TRY_PAUSE = 1 * time.Minute // test

/*
// Current network type
var NETWORK_TYPE = NetworkType.TestNet
var BALANCER_MODE = BalancerMode.Balance 
const LOGFILE = "log.txt"
const DEBUG_SEED = "159.89.252.210" // "159.89.252.210" // "159.89.252.210" // if not used set to empty string
const BALANCING_LOW_LIMIT = 0.01 // amount in BNB - if the required amount for pool balancing is smaller then we ignore the imbalance
const BNB_RESERVE = 1 // 1 BNB reserved for fees - at least this amount we want always keep in wallet
*/

// if TOKEN_MAPPING_ON == true, special chain combination is used - Thorchain testnet + BEP testnet for balancer wallet + BEP mainnet (DEX) for price feed
// in this case arbitrage is not possible, we can just balance pools with testnet tokens in testnet wallet to balance pools according to mainnet token prices
// testnet wallet must have enough testnet tokens for balancing
// TOKEN_MAPPING_ON is used only in Thorchain Testnet mode. In Chaosnet or Mainnet mode TOKEN_MAPPING_ON is ignored and everything runs on BEP mainnet
// const TOKEN_MAPPING_ON = false  
const TOKEN_MAPPING_ON = true

type Config struct {
    Wallet struct {
        Keystore	string `json:"keystore"`
        Password	string `json:"password"`
    } `json:"wallet"`
    NetworkType		int8 `json:"networktype"`
	BotMode			int8 `json:"balancermode"`
	LogFile			string `json:"logfile"`
	DebugSeed		string `json:"debugseed"`
	BlancingLimit	float64 `json:"balancinglimit"`
	Reserve			float64 `json:"bnbreserve"`
	CommentIgnored	[]string `json:"___comment"`
}

const (
	// Testnet:
	TestNetSeedURL			= "testnet-seed.thorchain.info"
	TestNetDexURL			= "testnet-dex.binance.org"
	// Chaosnet:
	ChaosNetSeedURL 		= "chaosnet-seed.thorchain.info"
	// Mainnet:
	MainNetSeedURL			= "seed.thorchain.info"
	MainNetDexURL			= "dex.binance.org"
)

func LoadConfiguration(file string) *Config {
    var config Config
    configFile, err := os.Open(file)
    defer configFile.Close()
    if err != nil {
		panic(err)
    }
    jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
    return &config
}
