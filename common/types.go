package common

import (
	"sync"
)

// Void type - void type
type Void struct{}

// type StringSet - set of strings
type StringSet map[string]Void

// PriceVol - map of volumes for prices
type PriceVol map[Fixed8]Fixed8

// BidAskSum - Orderbook Bids and Asks sums for one asset
type BidAskSum [3]Fixed8

// OrderBook - Orderbook on exchange
type OrderBook struct {
	Bids   PriceVol
	BidSum []BidAskSum
	Asks   PriceVol
	AskSum []BidAskSum
}

// OrderBooks - Orderbooks for various assets
type OrderBooks map[string]OrderBook

// IsEqual - OrderBook comparision
func (o OrderBook) IsEqual(o2 *OrderBook) bool {
	if len(o.Bids) != len(o2.Bids) || len(o.Asks) != len(o2.Asks) {
		return false
	}

	for k, v := range o.Bids {
		if v2, ok := o2.Bids[k]; !ok || v != v2 {
			return false
		}
	}
	return true
}

// OrderSide - BUY or SELL
var OrderSide = struct {
	BUY  int8
	SELL int8
}{1, 2}

// AssetStatusSync - 
type AssetStatusSync struct {
	mux		sync.Mutex
	status	int	// 0 - free, 1 - in balancing routine, 2 - getting orderbook, 3 - getting pool info
}

type AssetSyncMap map[string]*AssetStatusSync

func (as *AssetStatusSync) CanWorkOn(what int) bool {
	as.mux.Lock()
	defer as.mux.Unlock()
	if as.status != 0 {
		return false
	}
	as.status = what
	return true
}

func (as *AssetStatusSync) WorkDone() {
	as.mux.Lock()
	as.status = 0
	as.mux.Unlock()
}
